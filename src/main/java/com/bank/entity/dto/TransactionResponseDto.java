package com.bank.entity.dto;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionResponseDto {
	
		private Long transactionId;
		private Date transactionDate;
		private Double transactionAmount;
		private String transactionType;
		private Integer accountNo;

}
