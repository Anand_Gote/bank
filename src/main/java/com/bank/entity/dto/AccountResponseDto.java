package com.bank.entity.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountResponseDto {

	
	private Integer accountNo;
	private Double accountBalance;
	private String accountType;
	private Integer customerId;

}
