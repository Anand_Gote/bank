package com.bank.service;

import java.util.Date;
import java.util.List;
import com.bank.entity.dto.TransactionRequestDto;
import com.bank.entity.dto.TransactionResponseProjection;

public interface ITransactionService {

	public void doDebit(int accountNo, TransactionRequestDto transactionRequestDto);

	public void doDeposite(int accountNo, TransactionRequestDto transactionRequestDto);

	public void doTransfer(int accountNo1, int accountNo2, TransactionRequestDto transactionRequestDto);

	List<TransactionResponseProjection> getTransactionsByDate(Date fromDate, Date toDate);

}
