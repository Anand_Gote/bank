package com.bank.exception;

public class FailedTransactionException extends RuntimeException {

	public FailedTransactionException(String message) {
		super(message);

	}

}
