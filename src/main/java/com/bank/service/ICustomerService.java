package com.bank.service;

import java.util.List;

import com.bank.entity.dto.CustomerRequestDto;
import com.bank.entity.dto.CustomerResponseDto;

public interface ICustomerService {
	
	public void registerCustomer(CustomerRequestDto customerRequestDto);

	public List<CustomerResponseDto> displayCustomer();

	public CustomerResponseDto searchCustomer(Integer customerId);

	public void deleteCustomer(Integer customerId);

	public void updateCustomer(CustomerRequestDto customerRequestDto, Integer customerId);

	public List<CustomerResponseDto> displayCustomerByName(String customerName);



}
