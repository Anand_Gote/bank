package com.bank.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bank.entity.Account;
import com.bank.entity.Transaction;
import com.bank.entity.dto.AccountRequestDto;
import com.bank.entity.dto.TransactionRequestDto;
import com.bank.service.impl.AccountServiceImpl;
import com.bank.service.impl.TransactionServiceImpl;

@ExtendWith(MockitoExtension.class)
class TransactionControllerTest {

	@Mock
	TransactionServiceImpl transactionServiceImpl;

	@Mock
	AccountServiceImpl accountServiceImpl;

	@InjectMocks
	TransactionController transactionController;

	@InjectMocks
	AccountController accountController;

	Account account;

	AccountRequestDto accountRequestDTO;
	
	AccountRequestDto accountRequestDTO2;

	TransactionRequestDto transactionRequestDTO;

	Transaction transaction;

	Transaction transaction2;

	@BeforeEach
	public void setUp() {
		transactionRequestDTO = new TransactionRequestDto();
		transactionRequestDTO.setTransactionAmount(777777.7);
		transactionRequestDTO.setTransactionType("Transfer");

		transaction = new Transaction();
		transaction.setTransactionAmount(333.7);
		transaction.setTransactionType("debit");

		transaction2 = new Transaction();
		transaction2.setTransactionAmount(444.7);
		transaction2.setTransactionType("credit");

		account = new Account();
		account.setAccountNo(2);
		account.setAccountBalance(2000.0);

		accountRequestDTO = new AccountRequestDto();
		accountRequestDTO.setAccountNo(4);
		accountRequestDTO.setAccountBalance(4000.0);
		
		accountRequestDTO2 = new AccountRequestDto();
		accountRequestDTO2.setAccountNo(4);
		accountRequestDTO2.setAccountBalance(4000.0);

		transaction = new Transaction();
	}

	@Test
	void doDeposite() {

		accountServiceImpl.searchAccount(accountRequestDTO.getAccountNo());
		accountRequestDTO.setAccountBalance(
				accountRequestDTO.getAccountBalance() + transactionRequestDTO.getTransactionAmount());
		accountServiceImpl.registerAccount(accountRequestDTO);
		transactionServiceImpl.doDeposite(accountRequestDTO.getAccountNo(), transactionRequestDTO);
	}

	@Test
	void doDebit() {
		accountServiceImpl.searchAccount(accountRequestDTO.getAccountNo());
		accountRequestDTO.setAccountBalance(
				accountRequestDTO.getAccountBalance() - transactionRequestDTO.getTransactionAmount());
		accountServiceImpl.registerAccount(accountRequestDTO);
		transactionServiceImpl.doDeposite(accountRequestDTO.getAccountNo(), transactionRequestDTO);
	}

	@Test
	void doTransfer() {
		
		accountServiceImpl.searchAccount(accountRequestDTO.getAccountNo());
		accountServiceImpl.searchAccount(accountRequestDTO2.getAccountNo());
		
		accountRequestDTO.setAccountBalance(
				accountRequestDTO.getAccountBalance() - transactionRequestDTO.getTransactionAmount());
		accountRequestDTO2.setAccountBalance(
				accountRequestDTO.getAccountBalance() + transactionRequestDTO.getTransactionAmount());
		
		accountServiceImpl.registerAccount(accountRequestDTO);
		transactionServiceImpl.doDeposite(accountRequestDTO.getAccountNo(), transactionRequestDTO);

	}

}
