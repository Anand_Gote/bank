package com.bank.repo;

import java.util.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bank.entity.Transaction;
import com.bank.entity.dto.TransactionResponseProjection;

@Repository
public interface ITransactionRepository extends CrudRepository<Transaction, Integer> {

	List<TransactionResponseProjection> findByTransactionDateBetween(Date fromDate, Date toDate);

}
