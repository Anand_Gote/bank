package com.bank.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bank.entity.Account;
import com.bank.entity.Transaction;
import com.bank.entity.dto.TransactionRequestDto;
import com.bank.repo.IAccountRepository;
import com.bank.repo.ITransactionRepository;

@ExtendWith(MockitoExtension.class)
class TransactionServiceImplTest {

	@Mock
	ITransactionRepository transactionRepository;

	@Mock
	IAccountRepository accountRepository;

	@InjectMocks
	TransactionServiceImpl transactionServiceImpl;

	Account account;
	Account account2;

	TransactionRequestDto transactionRequestDTO;

	Transaction transaction;

	Transaction transaction2;

	@BeforeEach
	public void setUp() {
		transactionRequestDTO = new TransactionRequestDto();
		transactionRequestDTO.setTransactionAmount(777777.7);
		transactionRequestDTO.setTransactionType("Transfer");

		transaction = new Transaction();
		transaction.setTransactionAmount(333.7);
		transaction.setTransactionType("debit");

		transaction2 = new Transaction();
		transaction2.setTransactionAmount(444.7);
		transaction2.setTransactionType("credit");

		account = new Account();
		account.setAccountNo(2);
		account.setAccountBalance(2000.0);

		account2 = new Account();
		account2.setAccountNo(4);
		account2.setAccountBalance(4000.0);

		transaction = new Transaction();
	}

	@Test
	void doDeposite() {

		accountRepository.findById(account.getAccountNo()).orElse(null);
		account.setAccountBalance(account.getAccountBalance() + transactionRequestDTO.getTransactionAmount());
		accountRepository.save(account);
		transactionRepository.save(transaction);
	}

	@Test
	void doDebit() {
		accountRepository.findById(account.getAccountNo()).orElse(null);
		account.setAccountBalance(account.getAccountBalance() - transactionRequestDTO.getTransactionAmount());
		accountRepository.save(account);
		transactionRepository.save(transaction);
	}

	@Test
	void doTransfer() {

		accountRepository.findById(account.getAccountNo()).orElse(null);
		accountRepository.findById(account2.getAccountNo()).orElse(null);
		account.setAccountBalance(account.getAccountBalance() - transactionRequestDTO.getTransactionAmount());

		account2.setAccountBalance(account2.getAccountBalance() + transactionRequestDTO.getTransactionAmount());
		accountRepository.save(account);
		transactionRepository.save(transaction);

	}

}
