package com.bank.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.entity.Account;
import com.bank.entity.Customer;
import com.bank.entity.Transaction;
import com.bank.entity.dto.AccountRequestDto;
import com.bank.entity.dto.AccountResponseDto;
import com.bank.entity.dto.TransactionRequestDto;
import com.bank.exception.AccountNotFoundException;
import com.bank.exception.CustomerNotFoundException;
import com.bank.repo.IAccountRepository;
import com.bank.repo.ICustomerRepository;
import com.bank.repo.ITransactionRepository;
import com.bank.service.IAccountService;

@Service
public class AccountServiceImpl implements IAccountService {

	@Autowired
	IAccountRepository accountRepository;

	@Autowired
	ICustomerRepository customerRepository;
	
	@Autowired
	ITransactionRepository transactionRepository;

	public AccountServiceImpl(IAccountRepository accountRepository, ICustomerRepository customerRepository) {
		this.accountRepository = accountRepository;
		this.customerRepository = customerRepository;

	}
	String message="Account doesn't exist for ID-> ";

	@Override
	public AccountResponseDto registerAccount(AccountRequestDto accountRequestDto) {

		Optional<Customer> optionalCustommer = customerRepository.findById(accountRequestDto.getCustomerId());

		if (optionalCustommer.isEmpty())
			throw new CustomerNotFoundException("Customer doesn't exist for ID-> " + accountRequestDto.getCustomerId());

		Account account = new Account();
		BeanUtils.copyProperties(accountRequestDto, account);
		accountRepository.save(account);

		AccountResponseDto accountResponseDto = new AccountResponseDto();
		BeanUtils.copyProperties(account, accountResponseDto);
		return accountResponseDto;

	}

	@Override
	public List<AccountResponseDto> displayAccount() {

		List<Account> list;
		List<AccountResponseDto> list1 = new ArrayList<>();

		list = accountRepository.findAll();
		list.forEach(data -> {
			AccountResponseDto accountResponseDto = new AccountResponseDto();
			BeanUtils.copyProperties(data, accountResponseDto);
			list1.add(accountResponseDto);
		});
		return list1;
	}

	@Override
	public AccountResponseDto searchAccount(Integer accountId) {
		Account account;
		AccountResponseDto accountResponseDto = new AccountResponseDto();

		Optional<Account> optionalAccount = accountRepository.findById(accountId);

		if (optionalAccount.isEmpty())
			throw new AccountNotFoundException(message + accountId);
		else
			account = optionalAccount.get();
		BeanUtils.copyProperties(account, accountResponseDto);
		return accountResponseDto;

	}

	@Override
	public void deleteAccount(Integer accountId) {
		Optional<Account> optionalAccount = accountRepository.findById(accountId);
		if (optionalAccount.isPresent())
			accountRepository.deleteById(accountId);
		else
			throw new AccountNotFoundException(message + accountId);
	}


}
