package com.bank.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Account {

	@Id
	private Integer accountNo;
	private Double accountBalance;
	private String accountType;
	
	private Integer customerId;

	
	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
	private List<Transaction> transaction=new ArrayList<Transaction>();
	//private Transaction transaction;
	

}
