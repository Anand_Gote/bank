package com.bank.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bank.entity.dto.AccountRequestDto;
import com.bank.entity.dto.AccountResponseDto;
import com.bank.entity.dto.TransactionRequestDto;
import com.bank.service.IAccountService;

@RestController
public class AccountController {

	@Autowired
	IAccountService accountService;

	@PostMapping(value = "/accounts")
	public ResponseEntity<AccountResponseDto> registerAccount(@Valid @RequestBody AccountRequestDto accountRequestDto) {
		AccountResponseDto responseDto = accountService.registerAccount(accountRequestDto);
		return new ResponseEntity<AccountResponseDto>(responseDto, HttpStatus.CREATED);
	}

	@GetMapping(value = "/accounts")
	public ResponseEntity<List<AccountResponseDto>> displayAccount() {
		return new ResponseEntity<List<AccountResponseDto>>(accountService.displayAccount(), HttpStatus.ACCEPTED);
	}

	@GetMapping(value = "/accounts/{accountNo}")
	public ResponseEntity<AccountResponseDto> searchAccount(
			@NotNull(message = "AccountNo cannot be empty") @PathVariable Integer accountNo) {
		return new ResponseEntity<AccountResponseDto>(accountService.searchAccount(accountNo), HttpStatus.ACCEPTED);
	}

	@DeleteMapping(value = "/accounts/{accountNo}")
	public ResponseEntity<String> deleteAccount(
			@NotNull(message = "AccountNo cannot be empty") @PathVariable Integer accountNo) {
		accountService.deleteAccount(accountNo);
		return new ResponseEntity<String>("Account deleted successfully", HttpStatus.ACCEPTED);
	}



}
