package com.bank.entity.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountRequestDto {
	
	@NotNull(message="Account No.can not be empty")
	private Integer accountNo;
	
	@NotNull(message="Balance can not be empty")
	@Min(value=1000,message="Minimum balance should be Rs.1000")
	private Double accountBalance;
	
	@NotEmpty(message="Account type can not be empty")
	private String accountType;
	
	@NotNull(message="CustomerId can not be empty")
	private Integer customerId;

}
