package com.bank.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.entity.Customer;
import com.bank.entity.dto.CustomerRequestDto;
import com.bank.entity.dto.CustomerResponseDto;
import com.bank.exception.CustomerNotFoundException;
import com.bank.repo.ICustomerRepository;
import com.bank.service.ICustomerService;

@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	ICustomerRepository customerRepository;

	public CustomerServiceImpl(ICustomerRepository customerRepository) {
		this.customerRepository = customerRepository;

	}
	
	String message="Customer is not present";

	@Override
	public void registerCustomer(CustomerRequestDto customerRequestDto) {

		Customer customer = new Customer();
		BeanUtils.copyProperties(customerRequestDto, customer);
		customerRepository.save(customer);

	}

	@Override
	public List<CustomerResponseDto> displayCustomer() {
		
		List<Customer> list;
		List<CustomerResponseDto> list1 = new ArrayList<>();

		list = customerRepository.findAll();
		list.forEach(data -> {
			CustomerResponseDto customerResponseDto = new CustomerResponseDto();
			BeanUtils.copyProperties(data, customerResponseDto);
			list1.add(customerResponseDto);
		});
		
		return list1;
	}

	@Override
	public CustomerResponseDto searchCustomer(Integer customerId) {
		Customer customer;
		CustomerResponseDto customerResponseDto = new CustomerResponseDto();
		Optional<Customer> optionalCustomer = customerRepository.findById(customerId);

		if (optionalCustomer.isEmpty())
			throw new CustomerNotFoundException(message);
		else
			customer = optionalCustomer.get();

		BeanUtils.copyProperties(customer, customerResponseDto);
		return customerResponseDto;
	}

	@Override
	public void deleteCustomer(Integer customerId) {

		Optional<Customer> optionalCustomer = customerRepository.findById(customerId);

		if (optionalCustomer.isPresent())
			customerRepository.deleteById(customerId);
		else
			throw new CustomerNotFoundException(message);

	}

	@Override
	public void updateCustomer(CustomerRequestDto customerRequestDto, Integer customerId) {

		Customer customer;
		Optional<Customer> optionalCustomer = customerRepository.findById(customerId);

		if (optionalCustomer.isEmpty())
			throw new CustomerNotFoundException(message);
		else
			customer = optionalCustomer.get();
		BeanUtils.copyProperties(customerRequestDto, customer);
		customerRepository.save(customer);
	}

	@Override
	public List<CustomerResponseDto> displayCustomerByName(String customerName) {
		
		List<Customer> list;
		List<CustomerResponseDto> list1 = new ArrayList<>();

		list = customerRepository.findByName(customerName);
		if(list.isEmpty())throw new CustomerNotFoundException("Their is no customer present named  "+customerName);
		list.forEach(data -> {
			CustomerResponseDto customerResponseDto = new CustomerResponseDto();
			BeanUtils.copyProperties(data, customerResponseDto);
			list1.add(customerResponseDto);
		});
		
		return list1;
	}

}
