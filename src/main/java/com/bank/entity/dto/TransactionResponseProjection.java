package com.bank.entity.dto;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;

public interface TransactionResponseProjection {

	Long getTransactionId();

	Double getTransactionAmount();

	@Value("#{target.account.accountNo}")
	Integer getAccountNo();

	String getTransactionType();

	Date getTransactionDate();

}
