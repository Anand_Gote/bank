package com.bank.controller;

import static org.mockito.Mockito.verify;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bank.entity.Account;
import com.bank.entity.Customer;
import com.bank.entity.dto.AccountRequestDto;
import com.bank.entity.dto.AccountResponseDto;
import com.bank.entity.dto.CustomerRequestDto;
import com.bank.service.impl.AccountServiceImpl;
import com.bank.service.impl.CustomerServiceImpl;

@ExtendWith(MockitoExtension.class)
 class AccountControllerTest {

	@Mock
	AccountServiceImpl accountServiceImpl;

	@Mock
	CustomerServiceImpl customerServiceImpl;

	@InjectMocks
	AccountController accountController;

	@InjectMocks
	CustomerController customerController;

	AccountRequestDto accountRequestDTO;

	CustomerRequestDto cutomerRequestDTO;

	Customer customer;

	AccountResponseDto accountResponseDTO;

	Optional<Customer> optionalAccount;
	Account account;

	@BeforeEach
	public void setUp() {
		accountRequestDTO = new AccountRequestDto();
		accountRequestDTO.setAccountNo(1);
		accountRequestDTO.setAccountBalance(5555.99);
		accountRequestDTO.setAccountType("SA");

		customer = new Customer();
		customer.setCustomerId(303);

		account = new Account();
		account.setAccountNo(2);
		account.setAccountBalance(5555.99);
		account.setAccountType("CA");
		account.setCustomerId(303);

	}

	@Test
	void registerAccount() {

		customerController.searchCustomer(customer.getCustomerId());
		verify(customerServiceImpl).searchCustomer(customer.getCustomerId());

		accountController.registerAccount(accountRequestDTO);
		verify(accountServiceImpl).registerAccount(accountRequestDTO);

	}

	@Test
	void shouldDisplayAccount() {
		accountController.displayAccount();
		verify(accountServiceImpl).displayAccount();
	}

	@Test
	void shouldDeleteAccount() {
		accountController.deleteAccount(account.getAccountNo());
		verify(accountServiceImpl).deleteAccount(account.getAccountNo());
	}

	@Test
	void shouldUpdateAccount() {

		accountController.searchAccount(account.getAccountNo());
		verify(accountServiceImpl).searchAccount(account.getAccountNo());

		accountController.registerAccount(accountRequestDTO);
		verify(accountServiceImpl).registerAccount(accountRequestDTO);

	}
}
