package com.bank.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.entity.Account;
import com.bank.entity.Transaction;
import com.bank.entity.dto.TransactionRequestDto;
import com.bank.entity.dto.TransactionResponseProjection;
import com.bank.exception.AccountNotFoundException;
import com.bank.exception.FailedTransactionException;
import com.bank.repo.IAccountRepository;
import com.bank.repo.ITransactionRepository;
import com.bank.service.ITransactionService;

@Service
public class TransactionServiceImpl implements ITransactionService {

	@Autowired
	ITransactionRepository transactionRepository;

	@Autowired
	IAccountRepository accountRepository;

	@Override
	public void doDeposite(int accountNo, TransactionRequestDto transactionRequestDto) {

		Account account;
		Transaction transaction = new Transaction();

		Optional<Account> optionalAccount = accountRepository.findById(accountNo);
		if (optionalAccount.isEmpty())
			throw new AccountNotFoundException("Account not present");
		else
			account = optionalAccount.get();

		Double amount = transactionRequestDto.getTransactionAmount();
		account.setAccountBalance(account.getAccountBalance() + amount);
		accountRepository.save(account);

		transaction.setTransactionAmount(amount);
		transaction.setAccount(account);
		transaction.setTransactionType("credit");

		BeanUtils.copyProperties(transactionRequestDto, transaction);
		transactionRepository.save(transaction);

	}

	@Override
	public void doDebit(int accountNo, TransactionRequestDto transactionRequestDto) {

		Account account;
		Transaction transaction = new Transaction();

		Optional<Account> optionalAccount = accountRepository.findById(accountNo);

		if (optionalAccount.isEmpty())
			throw new AccountNotFoundException("Account not present");
		else
			account = optionalAccount.get();

		Double amount = transactionRequestDto.getTransactionAmount();

		if (account.getAccountBalance() < amount) {
			throw new FailedTransactionException("Insufficient balance");
		}

		transaction.setTransactionAmount(amount);
		transaction.setAccount(account);
		transaction.setTransactionType("debit");

		account.setAccountBalance(account.getAccountBalance() - amount);
		accountRepository.save(account);
		BeanUtils.copyProperties(transactionRequestDto, transaction);
		transactionRepository.save(transaction);

	}

	@Override
	public void doTransfer(int accountNo1, int accountNo2, TransactionRequestDto transactionRequestDto) {

		Account account1;
		Account account2;

		if (accountNo1 == accountNo2) {
			throw new FailedTransactionException("Cannot transfer to the same account ");
		}

		Optional<Account> optionalAccount1 = accountRepository.findById(accountNo1);
		if (optionalAccount1.isEmpty())
			throw new AccountNotFoundException("Sender account not present");
		else
			account1 = optionalAccount1.get();
		Double amount1 = transactionRequestDto.getTransactionAmount();
		if (account1.getAccountBalance() < amount1) {
			throw new FailedTransactionException("Insufficient balance");
		}
		Transaction transaction1 = new Transaction();
		transaction1.setTransactionAmount(amount1);
		transaction1.setAccount(account1);
		//transaction1.setTransactionType("debit");
		account1.setAccountBalance(account1.getAccountBalance() - amount1);
		accountRepository.save(account1);
		BeanUtils.copyProperties(transactionRequestDto, transaction1);
		transactionRepository.save(transaction1);

		Optional<Account> optionalAccount2 = accountRepository.findById(accountNo2);
		if (optionalAccount2.isEmpty())
			throw new AccountNotFoundException("Reciever account not present");
		else
			account2 = optionalAccount2.get();
		Double amount2 = transactionRequestDto.getTransactionAmount();
		Transaction transaction2 = new Transaction();

		transaction2.setTransactionAmount(amount2);
		transaction2.setAccount(account2);
		//transaction2.setTransactionType("credit");
		account2.setAccountBalance(account2.getAccountBalance() + amount2);
		accountRepository.save(account2);

		BeanUtils.copyProperties(transactionRequestDto, transaction2);
		transactionRepository.save(transaction2);

	}

	@Override
	public List<TransactionResponseProjection> getTransactionsByDate(Date fromDate, Date toDate) {
		if (fromDate.after(toDate))
			throw new FailedTransactionException("fromDate and toDate are wrong");

		List<TransactionResponseProjection> transactionList;
		transactionList = transactionRepository.findByTransactionDateBetween(fromDate, toDate);

		if (transactionList.isEmpty())
			throw new FailedTransactionException("No transaction for the given dates");
		else
			return transactionList;
	}

}
