package com.bank.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long transactionId;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date transactionDate;
	private Double transactionAmount;
	private String transactionType;

	@ManyToOne
	@JoinColumn(name = "accountNo")
	private Account account;

//	public Transaction(Date date, Double transactionAmount, String transactionType, Account account) {
//		super();
//	}

	public Transaction() {
		super();
	}

}
