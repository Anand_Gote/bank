package com.bank.service.impl;

import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bank.entity.Customer;
import com.bank.entity.dto.CustomerRequestDto;
import com.bank.repo.ICustomerRepository;

@ExtendWith(MockitoExtension.class)
 class CustomerServiceImplTest {

	@Mock
	ICustomerRepository customerRepository;

	@InjectMocks
	CustomerServiceImpl customerServiceImpl;

	CustomerRequestDto customerRequestDTO;

	Customer customer;

	Customer savedCustomer;

	@BeforeEach
	public void setUp() {
		customerRequestDTO = new CustomerRequestDto();
		customerRequestDTO.setCustomerName("Niks");
		customerRequestDTO.setCustomerPhone(777777);
		customerRequestDTO.setCustomerAddress("Pune");

		customer = new Customer();
		customer.setCustomerName("Raj");
		customer.setCustomerPhone(788888886);
		customer.setCustomerAddress("Bengaluru");

		savedCustomer = new Customer();
		savedCustomer.setCustomerName("Sam");
		savedCustomer.setCustomerPhone(788888886);
		savedCustomer.setCustomerAddress("Bengaluru");
		savedCustomer.setCustomerId(1);

		this.customerServiceImpl = new CustomerServiceImpl(this.customerRepository);
	}

//	@Test
//	public void shouldRegisterCustomer() {
//		when(customerRepository.save(any(Customer.class))).thenAnswer(i -> {
//			Customer customer = i.getArgument(0);
//			customer.setCustomerId(1);
//			return customer;
//		});
//		customerServiceImpl.registerCustomer(customerRequestDTO);
//	}

	@Test
	void shouldDisplayCustomer() {
		customerServiceImpl.displayCustomer();
		verify(customerRepository).findAll();
	}

//	@Test
//	void shouldDeleteCustomerById() {
////		CustomerResponseDto optionalCustomer = customerServiceImpl.searchCustomer(savedCustomer.getCustomerId());
////		verify(customerRepository).findById(savedCustomer.getCustomerId()); 
////		
//		
//		customerServiceImpl.deleteCustomer(savedCustomer.getCustomerId());
//		verify(customerRepository).deleteById(savedCustomer.getCustomerId());
//	}

//	@Test
//	void shouldUpdateCustomer() {
//		customerServiceImpl.searchCustomer(savedCustomer.getCustomerId());
//		verify(customerRepository).findById(savedCustomer.getCustomerId());
//
//		when(customerRepository.save(any(Customer.class))).thenAnswer(i -> {
//			Customer customer = i.getArgument(0);
//			customer.setCustomerId(1);
//			return customer;
//		});
//		customerServiceImpl.registerCustomer(customerRequestDTO);
//	}
}
