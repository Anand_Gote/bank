package com.bank.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.entity.dto.CustomerRequestDto;
import com.bank.entity.dto.CustomerResponseDto;
import com.bank.service.ICustomerService;

@RestController
class CustomerController {
	
	Logger logger = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	ICustomerService customerService;

	@PostMapping(value = "/customers")
	public ResponseEntity<String> registerCustomer(@Valid @RequestBody CustomerRequestDto customerRequestDto) {
		customerService.registerCustomer(customerRequestDto);
		logger.trace("RegisterCustomer called");
		return new ResponseEntity<String>("customer added succesfully", HttpStatus.ACCEPTED);
	}

	@GetMapping(value = "/customers")
	public ResponseEntity<List<CustomerResponseDto>> displayCustomer() {
		return new ResponseEntity<List<CustomerResponseDto>>(customerService.displayCustomer(), HttpStatus.ACCEPTED);

	}

	@GetMapping(value = "/customers/{customerId}")
	public ResponseEntity<CustomerResponseDto> searchCustomer(@PathVariable Integer customerId) {
		return new ResponseEntity<CustomerResponseDto>(customerService.searchCustomer(customerId), HttpStatus.ACCEPTED);

	}

	@DeleteMapping(value = "/customers/{customerId}")
	public ResponseEntity<String> deleteCustomer(
			@NotNull(message = "customerId cannot be null") @PathVariable Integer customerId) {
		customerService.deleteCustomer(customerId);
		return new ResponseEntity<String>("Customer deleted successfully", HttpStatus.ACCEPTED);
	}

	@PutMapping(value = "/customers/{customerId}")
	public ResponseEntity<String> updateCustomer(@RequestBody CustomerRequestDto customerRequestDto,
			@NotNull(message = "customerId cannot be null") @PathVariable Integer customerId) {
		customerService.updateCustomer(customerRequestDto, customerId);
		return new ResponseEntity<String>("Customer updated successfully", HttpStatus.ACCEPTED);
	}
	
	@GetMapping(value = "/customers/name")
	public ResponseEntity<List<CustomerResponseDto>> displayCustomerByName(@RequestParam String customerName) {
		return new ResponseEntity<List<CustomerResponseDto>>(customerService.displayCustomerByName(customerName), HttpStatus.ACCEPTED);

	}

}
