package com.bank.entity.dto;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Validated
public class CustomerRequestDto {

	@NotEmpty(message="Customer name cannot be empty")
	private String customerName;
	
	@NotNull(message="Customer phone cannot be empty")
	private Integer customerPhone;
	
	@NotEmpty(message="Customer address cannot be empty")
	private String customerAddress;

}
