package com.bank.entity.dto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class CustomerResponseDto {

	private Integer customerId;
	private String customerName;
	private Integer customerPhone;
	private String customerAddress;

}
