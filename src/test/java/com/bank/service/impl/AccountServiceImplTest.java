package com.bank.service.impl;

import static org.mockito.Mockito.verify;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;

import com.bank.entity.Account;
import com.bank.entity.Customer;
import com.bank.entity.dto.AccountRequestDto;
import com.bank.entity.dto.AccountResponseDto;
import com.bank.entity.dto.CustomerRequestDto;
import com.bank.entity.dto.CustomerResponseDto;
import com.bank.exception.AccountNotFoundException;
import com.bank.repo.IAccountRepository;
import com.bank.repo.ICustomerRepository;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

	@Mock
	IAccountRepository accountRepository;

	@Mock
	ICustomerRepository customerRepository;

	@InjectMocks
	AccountServiceImpl accountServiceImpl;

	@InjectMocks
	CustomerServiceImpl customerServiceImpl;

	AccountRequestDto accountRequestDTO;

	CustomerRequestDto cutomerRequestDTO;

	CustomerResponseDto customerResponseDto;

	Customer customer;

	AccountResponseDto accountResponseDTO;

	Optional<Customer> optionalCustomer;
	Optional<Account> optionalAccount;
	Account account;

	@BeforeEach
	public void setUp() {
		accountRequestDTO = new AccountRequestDto();
		accountRequestDTO.setAccountNo(1);
		accountRequestDTO.setAccountBalance(5555.99);
		accountRequestDTO.setAccountType("SA");

		customer = new Customer();
		customer.setCustomerId(303);

		cutomerRequestDTO = new CustomerRequestDto();

		account = new Account();
		account.setAccountNo(2);
		account.setAccountBalance(5555.99);
		account.setAccountType("CA");
		account.setCustomerId(2);

	}

//	@Test
//	void registerAccount() {
//
//		optionalCustomer = customerRepository.findById(customer.getCustomerId());
//		if (optionalCustomer.isPresent())
//
//			accountServiceImpl.registerAccount(accountRequestDTO);
//		verify(accountRepository).save(account);
//	}

	@Test
	void shouldDisplayAccount() {
		accountServiceImpl.displayAccount();
		verify(accountRepository).findAll();
	}

//	@Test
//	void shouldDeleteAccount() {
//	
//		int num=account.getAccountNo();
//
//		AccountNotFoundException thrown = Assertions.assertThrows(AccountNotFoundException.class, () -> {
//			accountServiceImpl.searchAccount(num);
//		});
//		System.out.println(thrown.getMessage());
//		
//		Assertions.assertEquals("Account doesn't exist for ID-> 2", thrown.getMessage());
//		accountServiceImpl.deleteAccount(account.getAccountNo());
//		verify(accountRepository).deleteById(account.getAccountNo());
//		
//	}

//	@Test
//	void shouldUpdateAccount() {
//		accountServiceImpl.searchAccount(account.getAccountNo());
//		verify(accountRepository).findById(account.getAccountNo());
//
//		when(accountRepository.save(any(Account.class))).thenAnswer(i -> {
//			Account account = i.getArgument(0);
//			return account;
//		});
//		accountServiceImpl.registerAccount(accountRequestDTO);
//	}
}
