package com.bank.exception;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationErrorResponse extends ErrorResponse {
	
	private Map<String,String> errors=new HashMap<String,String>();
	
	}
