package com.bank.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.entity.dto.TransactionRequestDto;
import com.bank.entity.dto.TransactionResponseProjection;
import com.bank.service.ITransactionService;

@RestController
public class TransactionController {

	@Autowired
	ITransactionService transactionService;

	@PostMapping("transactions/credit")
	public ResponseEntity<String> doDeposite(@RequestParam int accountNo,
			@RequestBody TransactionRequestDto transactionRequestDto) {
		transactionService.doDeposite(accountNo, transactionRequestDto);
		return new ResponseEntity<String>("Ammount credited Succesfully", HttpStatus.ACCEPTED);
	}

	@PostMapping("transactions/debit")
	public ResponseEntity<String> doDebit(@RequestParam int accountNo,
			@RequestBody TransactionRequestDto transactionRequestDto) {
		transactionService.doDebit(accountNo, transactionRequestDto);
		return new ResponseEntity<String>("Ammount debited Succesfully", HttpStatus.ACCEPTED);
	}

	@PostMapping("transactions/transfer")
	public ResponseEntity<String> doTransfer(@RequestParam int senderAccount, @RequestParam int recieverAccount,
			@RequestBody TransactionRequestDto transactionRequestDto) {
		transactionService.doTransfer(senderAccount, recieverAccount, transactionRequestDto);
		return new ResponseEntity<String>("Ammount transfered Succesfully", HttpStatus.ACCEPTED);

	}

	@GetMapping("/transactions/dates")
	public ResponseEntity<List<TransactionResponseProjection>> getTransactionsByDate(
			@RequestParam("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date fromDate,
			@RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date toDate) {

		return new ResponseEntity<List<TransactionResponseProjection>>(
				transactionService.getTransactionsByDate(fromDate, toDate), HttpStatus.ACCEPTED);
	}

}
