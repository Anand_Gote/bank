package com.bank.service;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.bank.entity.dto.AccountRequestDto;
import com.bank.entity.dto.AccountResponseDto;
import com.bank.entity.dto.TransactionRequestDto;

public interface IAccountService {

	AccountResponseDto registerAccount(AccountRequestDto accountRequestDto);

	List<AccountResponseDto> displayAccount();

	AccountResponseDto searchAccount(Integer accountNo);

	void deleteAccount(Integer accountNo);



}
