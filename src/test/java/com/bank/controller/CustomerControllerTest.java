package com.bank.controller;

import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bank.entity.Customer;
import com.bank.entity.dto.CustomerRequestDto;
import com.bank.service.impl.CustomerServiceImpl;

@ExtendWith(MockitoExtension.class)
class CustomerControllerTest {

	@Mock
	CustomerServiceImpl customerServiceImpl;

	@InjectMocks
	CustomerController customerController;

	CustomerRequestDto customerRequestDTO;

	Customer customer;

	@BeforeEach
	public void setUp() {
		customerRequestDTO = new CustomerRequestDto();
		customerRequestDTO.setCustomerName("Niks");
		customerRequestDTO.setCustomerPhone(88888888);
		customerRequestDTO.setCustomerAddress("Pune");

		customer = new Customer();
		customer.setCustomerName("Rock");
		customer.setCustomerPhone(44444);
		customer.setCustomerAddress("Bombay");
		customer.setCustomerId(32);

	}

	@Test
	@DisplayName("Register Customer")
	void shouldRegisterCustomer() {

		customerController.registerCustomer(customerRequestDTO);
		verify(customerServiceImpl).registerCustomer(customerRequestDTO);

	}

	@Test
	@DisplayName("Display Customer")
	void shouldDisplayCustomer() {

		customerController.displayCustomer();
		verify(customerServiceImpl).displayCustomer();
	}

	@Test
	@DisplayName("Delete Customer")
	void shouldDeleteCustomerById() {

		customerController.deleteCustomer(customer.getCustomerId());
		verify(customerServiceImpl).deleteCustomer(customer.getCustomerId());
		
	}

	@Test
	@DisplayName("Update Customer")
	void shouldUpdateCustomer() {
		
		customerController.searchCustomer(customer.getCustomerId());
		customerServiceImpl.searchCustomer(customer.getCustomerId());
		
		customerController.registerCustomer(customerRequestDTO);
		verify(customerServiceImpl).registerCustomer(customerRequestDTO);

	}

}
