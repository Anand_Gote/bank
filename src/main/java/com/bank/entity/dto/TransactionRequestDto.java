package com.bank.entity.dto;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionRequestDto {
	
	
	@NotNull(message="Transaction Date cannot be empty")
	private Date transactionDate;
	@NotNull(message="transaction amount cannot be empty")
	private Double transactionAmount;
	@NotEmpty(message="Transaction type cannot be empty")
	private String transactionType;
//	@NotNull(message="Transaction Account cannot be empty")
//	private Integer accountNo;

}
